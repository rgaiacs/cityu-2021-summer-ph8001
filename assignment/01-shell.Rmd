# Task 1

The current directory is:


```python
pwd
```

    /o1_d2/ph8001_user/rcostadas2


Let's copy the data file `/home/lijun/ph8001_homework/to_count.fa` to the current directory preserving the file's attributes:


```python
cp \
    --preserve \
    /home/lijun/ph8001_homework/to_count.fa \
    .
```

Let's check that the current directory has a file named `to_count.fa`:


```python
ls -l to_count.fa
```

    -rwxrwxr-x 1 rcostadas2 ph8001 58293 Jul  9 09:24 to_count.fa


Let's count the number of lines in `to_count.fa` and the number of fasta sequences, lines initiated with `>`:


```python
grep \
    -P '^>' \
    to_count.fa \
    | wc -l
```

    62


In the previous call to `grep`,
`-P` enables Perl-compatible regular expressions
and
`^` in the regular expression matches the start of the line.

Let's count how many lines contain the pattern "CTCTTATTA":


```python
grep \
    'CTCTTATTA' to_count.fa \
    | wc -l
```

    2


Let's change the accession permission of the file `to_count.fa` to readable and executable for our own user only:


```python
chmod 700 to_count.fa
```

Let's check that the file `to_count.fa` is readable and executable for our own user only:


```python
ls -l to_count.fa
```

    -rwx------ 1 rcostadas2 ph8001 58293 Jul  9 09:24 to_count.fa

